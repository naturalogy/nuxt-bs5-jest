export default {
  title: 'CommonButton',
  argTypes: {
    type: {
      options: ['button', 'submit', 'reset'],
      control: { type: 'inline-radio' },
    },
    variant: {
      options: ['primary', 'secondary', 'info', 'success', 'warning', 'danger', 'dark', 'light'],
      control: { type: 'inline-radio' },
    },
    size: {
      options: ['sm', 'md', 'lg'],
      control: { type: 'inline-radio' },
    },
    disabled: {
      control: { type: 'boolean' },
    },
  },
}

// Default
export const Default = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<Button v-bind="$props">Button</Button>',
});

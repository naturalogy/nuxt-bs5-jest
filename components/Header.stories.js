export default {
  title: 'Header',
  argTypes: {
    variant: {
      options: ['primary', 'secondary'],
      control: { type: 'radio' }
    }
  }
}

export const CommonHeader = () => '<Header />'

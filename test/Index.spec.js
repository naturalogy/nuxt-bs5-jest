import { shallowMount } from '@vue/test-utils'
import Home from '@/pages/index.vue'

describe('pages/index.vue', () => {
  const wrapper = shallowMount(Home)

  it('is shown as expected', () => {
    expect(wrapper.element.tagName).toBe('SECTION')
  })

  it('has a section heading', async () => {
    const sectionHeading = wrapper.find('h1')

    await wrapper.setData({
      title: 'nuxt-bs5-jest',
    })
    expect(sectionHeading.classes()).toContain('section-heading')
    expect(sectionHeading.text()).toBe('nuxt-bs5-jest')
  })

  it('has a trigger for modal', () => {
    const btnModal = wrapper.find('button[data-bs-target="#exampleModal"]')

    expect(btnModal.attributes('type')).toBe('button')
    expect(btnModal.attributes('variant')).toContain('primary')
    expect(btnModal.text()).toBe('モーダルを開く')
  })
})

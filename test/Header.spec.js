import { mount, RouterLinkStub } from '@vue/test-utils'
import Header from '@/components/Header.vue'

describe('components/Header.vue', () => {
  const wrapper = mount(Header, {
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  })

  it('is shown as expected', () => {
    expect(wrapper.element.tagName).toBe('HEADER')
    expect(wrapper.classes()).toContain('fixed-top')
  })

  it('has a brand element', async () => {
    const brand = wrapper.find('.navbar-brand')

    await wrapper.setData({
      title: 'nuxt-bs5-jest',
    })
    expect(brand.element.tagName).toBe('A')
    expect(brand.props('to')).toBe('/')
    expect(brand.text()).toBe('nuxt-bs5-jest')
  })
})

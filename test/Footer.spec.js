import { mount } from '@vue/test-utils'
import Footer from '@/components/Footer.vue'

describe('components/Footer.vue', () => {
  const wrapper = mount(Footer)

  it('is shown as expected', () => {
    expect(wrapper.element.tagName).toBe('FOOTER')
  })

  it('has a copyright line', async () => {
    const copy = wrapper.find('.copyright')

    await wrapper.setData({
      title: 'nuxt-bs5-jest',
    })
    expect(copy.text()).toContain('nuxt-bs5-jest')
    expect(copy.find('time').text()).toBe('2021')
    expect(copy.find('time').attributes('datetime')).toBe('2021')
  })
})
